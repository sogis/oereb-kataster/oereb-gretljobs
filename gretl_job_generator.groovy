// TODO:
// * custom properties, e.g. for cron
// * custom init.gradle in Job-Ordner steuert GRETL-Version etc.

println "BUILD_NUMBER = ${BUILD_NUMBER}"

// set default values
def gretlJobFilePath = '**'
def gretlJobFileName = 'build.gradle'
def jenkinsfileName = 'Jenkinsfile'
def jobPropertiesFileName = 'job.properties'

def baseDir = SEED_JOB.getWorkspace().toString()
println 'base dir: ' + baseDir

// search for GRETL-Job (Gradle) scripts (gretlJobFileName)
def jobFilePattern = "${gretlJobFilePath}/${gretlJobFileName}"
println 'job file pattern: ' + jobFilePattern

def jobFiles = new FileNameFinder().getFileNames(baseDir, jobFilePattern)

// generate the jobs
println 'generating the jobs...'
for (jobFile in jobFiles) {

  def relativeScriptPath = (jobFile - baseDir).substring(1)
  def _jobPath = relativeScriptPath.split('/')

  // take last folder for job name
  def namePosition = _jobPath.size() > 1 ? _jobPath.size() - 2 : 0
  def jobName = _jobPath[namePosition]
  println 'Job ' + jobName
  println 'script file: ' + relativeScriptPath

  def pipelineFilePath = "${baseDir}/${jenkinsfileName}"

  // set defaults for job properties
  def properties = new Properties([
    'authorization.permissions':'nobody',
    'logRotator.numToKeep':'15',
    'parameters.fileParam':'none',
    'parameters.stringParam':'none',
    'triggers.upstream':'none',
    'triggers.cron':'',
    'gradle.continue':'false'
  ])
  def propertiesFilePath = "${jobName}/${jobPropertiesFileName}"
  def propertiesFile = new File(baseDir, propertiesFilePath)
  if (propertiesFile.exists()) {
    println 'properties file found: ' + propertiesFilePath
    properties.load(propertiesFile.newDataInputStream())
  }


/*
  // check if job provides its own Jenkinsfile
  def customPipelineFilePath = "${jobName}/${jenkinsfileName}"
  if (new File(baseDir, customPipelineFilePath).exists()) {
    pipelineFilePath = customPipelineFilePath
    println 'custom pipeline file found: ' + customPipelineFilePath
  }
  def pipelineScript = readFileFromWorkspace(pipelineFilePath)

  // set defaults for job properties
  def properties = new Properties([
    'authorization.permissions':'nobody',
    'logRotator.numToKeep':'15',
    'parameters.fileParam':'none',
    'parameters.stringParam':'none',
    'triggers.upstream':'none',
    'triggers.cron':''
  ])
  def propertiesFilePath = "${jobName}/${jobPropertiesFileName}"
  def propertiesFile = new File(baseDir, propertiesFilePath)
  if (propertiesFile.exists()) {
    println 'properties file found: ' + propertiesFilePath
    properties.load(propertiesFile.newDataInputStream())
  }
*/
    def gitUrl = "https://gitlab.com/sogis/oereb-kataster/oereb-gretljobs.git"

    def gradleCmd = 'GRADLE_OPTS="-Xms512m -Xmx2048m" ./gradlew --init-script $PWD/init.gradle -p '+jobName+'/ --no-daemon -i '

    if (properties.getProperty('gradle.continue') == 'true') {
      gradleCmd = gradleCmd + '--continue'
    }

    job(jobName) {
        scm {
            git {
                remote {
                    url(gitUrl)
                }         
                branch('master')
            }
        }
        triggers {
            //cron('H/15 * * * *')
            //cron('@daily')
            cron(properties.getProperty('triggers.cron'))
        }
        steps {
            shell(gradleCmd)
        }
    }

}  