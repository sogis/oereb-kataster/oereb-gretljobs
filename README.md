# oereb-gretljobs


```
export ORG_GRADLE_PROJECT_dbUriOereb="jdbc:postgresql://localhost:54321/oereb"
export ORG_GRADLE_PROJECT_dbUserOereb="gretl"
export ORG_GRADLE_PROJECT_dbPwdOereb="gretl"
```


## Jenkins Job Generator

FIXME: 
- Freestyle-Job `gretl-job-generator-oereb`
- gitrepo `https://gitlab.com/sogis/oereb-kataster/oereb-gretljobs.git`
- Build - Add build step - Process Job DSL
- Look on Filesystem: `gretl_job_generator.groovy`
- TODO: beste Lösung für Sandbox, script approval. Plugin ist installiert aber irgendwie nicht sichtbar. Zum Entwickeln "Configure Global Security" - CSRF Protection - Enable script security for Job DSL scripts ausschalten.

Dokumentation Job DSL: https://jenkinsci.github.io/job-dsl-plugin/


## TODO etc
- Es können die "im Prinzip" Originaljobs verwendet werden. Es braucht aber ein `init.gradle` File. Bei den kantonalen ÖREB-Daten braucht es aber andere Jobs, da hier nicht umgebaut wird (werden kann/soll), sondern nur importiert.
- WMS-Servernamen ersetzen?